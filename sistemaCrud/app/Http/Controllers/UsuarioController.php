<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['usuarios'] = Usuario::paginate(10);
        return view('usuarios.index',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $campos = [
            'name'=>'required|alpha|String|max:191',
            'email'=>'required|email|max:191',
            'foto'=>'required|max:10000|mimes:jpeg,png,jpg'
        ];

        $mensaje =[
            'required'=>'El :attribute es requerido',
            'foto.required'=>'La Foto es requerida'
        ];

        $this->validate($request, $campos, $mensaje);

        $datosUsuario = request()->except('_token');

        if($request->hasFile('foto')){
            $datosUsuario['foto']=$request->file('foto')->store('uploads','public');
        }
        
        Usuario::insert($datosUsuario);
        //return response()->json($datosUsuario);
        return redirect('usuarios')->with('mensaje','Usuario agregado Correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show(Usuario $usuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $usuario = Usuario::findOrFail($id);
        return view('usuarios.edit', compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $campos = [
            'name'=>'required|alpha|string|max:191',
            'email'=>'required|email|max:191',
        ];

        $mensaje =[
            'required'=>'El :attribute es requerido',
        ];

        if($request->hasFile('foto')){
            $campos = ['foto'=>'required|max:10000|mimes:jpeg,png,jpg'];
            $mensaje =['foto.required'=>'La Foto es requerida'];
        }

        $this->validate($request, $campos, $mensaje);

        $datosUsuario = request()->except(['_token','_method']);
        
        if($request->hasFile('foto')){
            $usuario = Usuario::findOrFail($id);

            Storage::delete('public/'.$usuario->foto);

            $datosUsuario['foto']=$request->file('foto')->store('uploads','public');
        }

        Usuario::where('id','=',$id)->update($datosUsuario);

        $usuario = Usuario::findOrFail($id);
        //return view('usuarios.edit', compact('usuario'));
        return redirect('usuarios')->with('mensaje','Usuario editado Correctamente');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $usuario = Usuario::findOrFail($id);
        if(Storage::delete('public/'.$usuario->foto)){

            Usuario::destroy($id);
        }
        
        return redirect('usuarios')->with('mensaje','Usuario eliminado Correctamente');;
    }
}
