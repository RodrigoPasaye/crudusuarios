<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

/*Route::get('/usuarios', function () {
    return view('usuarios.index');
});

Route::get('/usuarios/create', [UsuarioController::class,'create']);


Descomentado

Route::resource('usuarios', UsuarioController::class)->middleware('auth');

Auth::routes(['register'=>false,'reset'=>false]);

Route::get('/home', [UsuarioController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function () {
    
    Route::get('/', [UsuarioController::class, 'index'])->name('home');
    
});
NUEVO
*/

Route::resource('usuarios', UsuarioController::class)->middleware(['auth','admin']);

Auth::routes(['register'=>false,'reset'=>false]);

Route::get('/home', [UsuarioController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth','admin']], function () {
    
    Route::get('/', [UsuarioController::class, 'index'])->name('home');
    Route::get('/usuarios/create', [UsuarioController::class, 'create']);
    
});
