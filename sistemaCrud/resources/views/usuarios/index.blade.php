@extends('layouts.app')

@section('content')
<div class="container">

@if(Session::has('mensaje'))
    <div class="alert alert-success alert-dismissible" role="alert">
        
        {{ Session::get('mensaje')}}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<br>
<div class="text-center">
    <h1>Sistema de Administración de Usuarios</h1>
</div>
<br><br>
<a href="{{ url('usuarios/create') }}" class="btn btn-primary">
    Registrar Nuevo Usuario
</a>

<br/>
<br/>

<table class="table table-hover">
    <thead class="thead-light">
        <tr>
            <th>ID</th>
            <th>Foto</th>
            <th>Nombre</th>
            <th>Email</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($usuarios as $usuario)
        <tr>
            <td>{{ $usuario->id }}</td>

            <td>
                <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$usuario->foto }}" width="110px" alt="">
            </td>

            <td>{{ $usuario->name }}</td>
            <td>{{ $usuario->email }}</td>
            <td>

                <a href="{{ url('/usuarios/'.$usuario->id.'/edit') }}" class="btn btn-warning">
                    Editar
                </a>                

                <form action="{{ url('/usuarios/'.$usuario->id) }}" class="d-inline" method="post">
                @csrf
                    {{ method_field('DELETE') }}
                    <input class="btn btn-danger" type="submit" onclick="return confirm('¿Estás seguro de eliminar al usuario con el nombre: {{ $usuario->name }}?')" value="Eliminar">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div class="text-center">
    {!! $usuarios->links() !!}
</div>

</div>
@endsection