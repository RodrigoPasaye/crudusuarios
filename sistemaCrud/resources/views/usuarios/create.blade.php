@extends('layouts.app')

@section('content')
<div class="container">

<form action="{{ url('/usuarios') }}" method="POST" enctype="multipart/form-data">
@csrf
@include('usuarios.form',['modo'=>'Registrar'])
</form>

</div>
@endsection