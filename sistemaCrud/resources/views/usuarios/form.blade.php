<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<script src="{{ asset('js/app.js') }}"></script>

<h1>{{ $modo }} Usuario</h1>

@if(count($errors) > 0)

    <div class="alert alert-danger" role="alert">
        <ul>
            @foreach($errors->all() as $error)
                <li> {{ $error }} </li>
            @endforeach
        </ul>
    </div>

@endif

<div class="container">
<div class="form-group">
        <label for="name">Nombre: </label>
        <input class="form-control" type="text" name="name" value="{{ isset($usuario->name)?$usuario->name:old('name') }}" id="name">
    </div>
    <div class="form-group">
        <label for="email">Email: </label>
        <input class="form-control" type="email" name="email" value="{{ isset($usuario->email)?$usuario->email:old('email')  }}" id="email">
    </div>
    <div class="form-group">
        <label for="foto">Foto: </label>
        <br>
        
        @if(isset($usuario->foto))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$usuario->foto }}" width="110px" alt="">
        <br><br>
        @endif
        
        <input class="" type="file" name="foto" accept="image/*" value="" id="foto">
    </div>
    <br>
    <a class="btn btn-primary" href="{{ url('usuarios/') }}">
        Cancelar
    </a>
    <input class="btn btn-success" type="submit" value="{{ $modo }} Datos">
</div>