<?php

namespace Database\Seeders;
use Faker\Factory as Faker;
use Illuminate\Queue\Jobs\DatabaseJob;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Seeder;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();
            for ($i=0; $i < 15; $i++) {
                  DB::table("usuarios")->insert(
                        array(
                              'name' => $faker->firstNameMale,
                              'email'  => $faker->email,
                              'foto'  => $faker->imageUrl($width =640, $height=480, 'cats'),
                              'created_at' => date('Y-m-d H:m:s'),
                              'updated_at' => date('Y-m-d H:m:s')
                        )
                  );
            }
    }
}
